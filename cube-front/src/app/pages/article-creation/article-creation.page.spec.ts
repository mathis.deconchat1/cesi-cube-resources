import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleCreationPage } from './article-creation.page';

describe('ArticleCreationPage', () => {
  let component: ArticleCreationPage;
  let fixture: ComponentFixture<ArticleCreationPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleCreationPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleCreationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
