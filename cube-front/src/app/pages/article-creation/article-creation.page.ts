import {
  ICusCreateUpdatePostInput,
  ICusCreateTagInput,
} from '../../models/generated-maia.model'
import { CategoriesGraphQL } from './../../http/categories/categories.graphql';
import {
  ITag,
  ICategory,
  ICreatePostsTagInput,
} from '../../models/generated-maia.model';
import { TagsGraphQL } from './../../http/tags/tags.graphql';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostsGraphQL } from 'src/app/http/posts/posts.graphql';

@Component({
  selector: 'app-article-creation',
  templateUrl: './article-creation.page.html',
  styleUrls: ['./article-creation.page.scss'],
})
export class ArticleCreationPage implements OnInit {
  html: string = '';
  // Quill editor
  modules: any = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      ['blockquote', 'code-block'],

      [{ header: 1 }, { header: 2 }], // custom button values
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
      [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
      [{ direction: 'rtl' }], // text direction

      [{ size: ['small', false, 'large', 'huge'] }], // custom dropdown
      [{ header: [1, 2, 3, 4, 5, 6, false] }],

      [{ color: [] }, { background: [] }], // dropdown with defaults from theme
      [{ font: [] }],
      [{ align: [] }],

      ['clean'], // remove formatting button

      ['link', 'image', 'video'], // link and image, video
    ],
  };

  tags: ITag[] = [];
  selectedTags: ITag[] = [];
  categories: ICategory[] = [];

  constructor(
    private tagsGql: TagsGraphQL,
    private categoriesGql: CategoriesGraphQL,
    private postsGql: PostsGraphQL
  ) {}

  ngOnInit(): void {
    this.tagsGql.getTags().subscribe((tags) => {
      this.tags = tags.tags.nodes;
    });
    this.categoriesGql.getCategories().subscribe((cat) => {
      this.categories = cat.categories.nodes;
    });
  }

  postForm = new FormGroup({
    postTitle: new FormControl('', Validators.required),
    postContent: new FormControl('', Validators.required),
    postDescription: new FormControl('', Validators.required),
    categorieId: new FormControl(Validators.required),
    tagName: new FormControl(''),
  });

  onEnter() {
    let tagInput: ICusCreateTagInput = {
      iTag: {
        name: this.postForm.get('tagName')?.value,
      },
    };

    this.tagsGql.createTag(tagInput).subscribe((tag) => {
      this.selectedTags.push(tag.data.cusCreateTag.tag);
    });
    
    this.postForm.get('tagName')?.patchValue('');
  }

  deleteFromTags(tag: any){
    let index = this.selectedTags.findIndex(o => {
      o.id = tag.id
    })
    this.selectedTags.splice(index, 1);
  }

  onClick() {
    let tagsId: number[] = [];
    this.selectedTags.forEach(tag => {
      tagsId.push(tag.id)
    })
    let insertPost: ICusCreateUpdatePostInput = {
      iPost: {
        postTitle: this.postForm.get('postTitle')?.value,
        postDescription: this.postForm.get('postDescription')?.value,
        postContent: this.postForm.get('postContent')?.value,
        categorieId: Number(this.postForm.get('categorieId')?.value),
      },
      tagsId: tagsId

    };
    this.postsGql.createPost(insertPost).subscribe((cr) => {
      console.log(cr);
    });
  }
}
