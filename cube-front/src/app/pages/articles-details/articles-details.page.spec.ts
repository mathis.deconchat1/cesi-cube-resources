import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesDetailsPage } from './articles-details.page';

describe('ArticlesDetailsPage', () => {
  let component: ArticlesDetailsPage;
  let fixture: ComponentFixture<ArticlesDetailsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesDetailsPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
