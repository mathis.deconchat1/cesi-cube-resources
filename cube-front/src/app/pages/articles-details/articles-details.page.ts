import { IPost } from './../../models/generated-maia.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsGraphQL } from 'src/app/http/posts/posts.graphql';

@Component({
  selector: 'app-articles-details',
  templateUrl: './articles-details.page.html',
  styleUrls: ['./articles-details.page.scss']
})
export class ArticlesDetailsPage implements OnInit {

  constructor(
    private postsGql: PostsGraphQL,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getPost()
  }

  id: number | null = 0;
  post?: IPost;

  private getPost(): void{
    if (this.activatedRoute.snapshot.paramMap.get('id')){
      this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'))
      this.postsGql.getPostDetail(this.id)
      .subscribe((post) => {
        console.log(post.post)
        this.post = post.post
      })
    }
  }

}
