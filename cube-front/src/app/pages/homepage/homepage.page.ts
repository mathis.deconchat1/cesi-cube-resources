
import { Component, OnInit } from '@angular/core';
import { PostsGraphQL } from 'src/app/http/posts/posts.graphql';
import { IPost } from 'src/app/models/generated-maia.model';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss']
})
export class HomepagePage implements OnInit {

  constructor(
    private postsGraphQL: PostsGraphQL
  ) { }

  posts?: Partial<IPost>[]

  ngOnInit(): void {
    this.getPartialPosts();
  }

  private getPartialPosts(){
    this.postsGraphQL.getPartialPosts()
    .subscribe(partialPosts => {
      console.log(partialPosts)
      this.posts = partialPosts.posts.nodes})
  }

}
