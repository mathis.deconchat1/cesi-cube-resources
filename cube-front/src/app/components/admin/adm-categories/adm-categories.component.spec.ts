import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmCategoriesComponent } from './adm-categories.component';

describe('AdmCategoriesComponent', () => {
  let component: AdmCategoriesComponent;
  let fixture: ComponentFixture<AdmCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
