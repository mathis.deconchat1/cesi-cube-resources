import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  ICategory,
  ICreateCategoryInput,
  IDeleteCategoryInput,
} from './../../../models/generated-maia.model';
import { CategoriesGraphQL } from './../../../http/categories/categories.graphql';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-adm-categories',
  templateUrl: './adm-categories.component.html',
  styleUrls: ['./adm-categories.component.scss'],
})
export class AdmCategoriesComponent implements OnInit {
  categories: ICategory[] = [];
  catSub: Subscription[] = [];

  constructor(private categoriesGql: CategoriesGraphQL) {}

  private getCat() {
    this.categoriesGql.getCategories().subscribe((cat) => {
      this.categories = cat.categories.nodes;
    });
    console.log(this.categories);
  }

  ngOnInit(): void {
    this.getCat()
  }

  catForm = new FormGroup({
    name: new FormControl(
      '',
      Validators.compose([Validators.required, Validators.min(10)])
    ),
    description: new FormControl(
      '',
      Validators.compose([Validators.required, Validators.min(10)])
    ),
  });

  public createCategory() {
    let catInput: ICreateCategoryInput = {
      category: {
        name: this.catForm.get('name')?.value,
        description: this.catForm.get('description')?.value,
      },
    };
    this.categoriesGql.createCategory(catInput).subscribe((cat) => {
      this.categories.push(cat.data.createCategory.category);
      console.log(cat);
    });
  }

  public deleteCategory(id: number) {

    this.catSub.forEach(sub => sub.unsubscribe())
    this.catSub.push(this.categoriesGql.deleteCategory(id).subscribe((cat) => {
      this.getCat();
    }))

    

  }
}
