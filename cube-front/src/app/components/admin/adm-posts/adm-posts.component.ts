import { IPost } from './../../../models/generated-maia.model';
import { PostsGraphQL } from './../../../http/posts/posts.graphql';
import { Component, OnInit } from '@angular/core';

export interface TablePosts {
  id?: number;
  title?: string;
  createdAt?: Date;
}

// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];
@Component({
  selector: 'app-adm-posts',
  templateUrl: './adm-posts.component.html',
  styleUrls: ['./adm-posts.component.scss']
})


export class AdmPostsComponent implements OnInit {

  constructor(
    private postsGraphQL: PostsGraphQL
  ) { }

  posts?: Partial<IPost>[];
  tablePosts: TablePosts[] = [];
  dataSource: any[] = []
  displayedColumns: string[] = ['id', 'postTitle', 'createdAt'];

  ngOnInit(): void {
    this.getPartialPosts();
    console.log(this.tablePosts)
  }

  private getPartialPosts(){
    this.postsGraphQL.getPartialPosts()
    .subscribe(partialPosts => {      
      this.tablePosts = this.convertPostToTableFormat(partialPosts.posts.nodes)
      this.dataSource = this.tablePosts;
    })      
      
  }


  private convertPostToTableFormat(posts: any){
    let pp: Partial<IPost>[] = []
    if(posts){
      posts.forEach((post: Partial<IPost>) => {
        pp?.push({
          id: post.id,
          postTitle: post?.postTitle,
          createdAt: post?.createdAt
        })
      })
    }
    console.log(pp)
    return pp;
  }


}
