import { FormControl, Validators, FormGroup } from '@angular/forms';
import { TagsGraphQL } from './../../../http/tags/tags.graphql';
import { ICusCreateTagInput, ITag } from './../../../models/generated-maia.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adm-tags',
  templateUrl: './adm-tags.component.html',
  styleUrls: ['./adm-tags.component.scss'],
})
export class AdmTagsComponent implements OnInit {
  tags: ITag[] = [];

  tagForm = new FormGroup({
    tagName: new FormControl('', Validators.required)
  })

  constructor(private tagsGql: TagsGraphQL) {}

  private getTags() {
    this.tagsGql.getTags().subscribe((tags) => {
      console.log(tags)
      this.tags = tags.tags.nodes;
    });
  }

  public createTag(){
    let tagInput: ICusCreateTagInput = {
      iTag: {
        name: this.tagForm.get('tagName')?.value,
      },
    };

    this.tagsGql.createTag(tagInput).subscribe((tag) => {
      this.getTags();
    });

    this.tagForm.get('tagName')?.patchValue('')
  }

  ngOnInit(): void {
    this.getTags();
  }

  deleteTag(id: number){
    this.tagsGql.deleteTag(id).subscribe(tag => {
      this.getTags()
    })
  }
}
