import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmTagsComponent } from './adm-tags.component';

describe('AdmTagsComponent', () => {
  let component: AdmTagsComponent;
  let fixture: ComponentFixture<AdmTagsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmTagsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
