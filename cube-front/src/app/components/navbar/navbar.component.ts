import { AuthGraphql } from './../../http/auth/auth.graphql';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private authGraphql: AuthGraphql) { }
  isVisible: boolean = false;

  ngOnInit(): void {
    this.authGraphql.isAdmin()
    .subscribe(e => {
      console.log(e)
      this.isVisible = e.isAdmin
    })
  }

}
