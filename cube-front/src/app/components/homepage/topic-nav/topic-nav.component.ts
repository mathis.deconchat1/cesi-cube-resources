import { ICategory } from './../../../models/generated-maia.model';
import { CategoriesGraphQL } from './../../../http/categories/categories.graphql';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topic-nav',
  templateUrl: './topic-nav.component.html',
  styleUrls: ['./topic-nav.component.scss']
})
export class TopicNavComponent implements OnInit {

  constructor(
    private categoriesGql: CategoriesGraphQL
  ) { }

  categories: ICategory[]= []
  ngOnInit(): void {
    this.categoriesGql.getCategories()
    .subscribe((cat) => {this.categories = cat.categories.nodes})
  }

  

}
