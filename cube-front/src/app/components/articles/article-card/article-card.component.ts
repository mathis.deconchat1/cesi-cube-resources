import { Component, Input, OnInit } from '@angular/core';
import { IPost } from 'src/app/models/generated-maia.model';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.scss']
})
export class ArticleCardComponent implements OnInit {

  constructor() { }

  @Input() post?: Partial<IPost>

  ngOnInit(): void {
  }

}
