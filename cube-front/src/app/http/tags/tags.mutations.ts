import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class tagsMutation {
  public createTag = gql`
    mutation createTag($tagInput: CusCreateTagInput!) {
      cusCreateTag(input: $tagInput) {
        tag {
          name
          id
        }
      }
    }
  `;

  public deleteTag = gql`
    mutation deleteTag($id: Int!) {
      deleteTag(input: { id: $id }) {
        tag {
          id
        }
      }
    }
  `;
}
