import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class TagsQuery {
  public getTags = gql`
    query tags {
      tags {
        nodes {
          id
          name
        }
      }
    }
  `;
}