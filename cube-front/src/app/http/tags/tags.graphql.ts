import { tagsMutation } from './tags.mutations';
import { ApolloQueryResult } from '@apollo/client/core';
import {ITagsConnection, ICusCreateTagInput } from '../../models/generated-maia.model'
import { TagsQuery } from './tags.query';
import { GraphQLService } from './../graphql.service';
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";

type tagsQueryResult = ApolloQueryResult<{ tags: ITagsConnection }>;


@Injectable({
    providedIn: "root"
})
export class TagsGraphQL {
    constructor(
        private graphqlService : GraphQLService,
        private tagsQuery : TagsQuery,
        private tagsMutation: tagsMutation
        )  {}

    public getTags() {
        return this.graphqlService.query(this.tagsQuery.getTags, {}, 'local')
        .pipe(map((tags) => tags.data));
    }

    public createTag(tagInput: ICusCreateTagInput){
        return this.graphqlService.mutate(this.tagsMutation.createTag, {tagInput}, 'local') 
    }

    public deleteTag(id: number){
        return this.graphqlService.mutate(this.tagsMutation.deleteTag, {id}, 'local')
    }
}