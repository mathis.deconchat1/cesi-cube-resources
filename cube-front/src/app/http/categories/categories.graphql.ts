import {  CategoriesMutation } from './categories.mutations';
import { ICreateCategoryInput, IDeleteCategoryInput } from './../../models/generated-maia.model';

import { CategoriesQuery } from './categories.query';
import { GraphQLService } from '../graphql.service';
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";



@Injectable({
    providedIn: "root"
})
export class CategoriesGraphQL {
    constructor(
        private graphqlService : GraphQLService,
        private categoriesQuery : CategoriesQuery,
        private categoriesMutation: CategoriesMutation
        )  {}

    public getCategories() {
        return this.graphqlService.query(this.categoriesQuery.getCategories, {}, 'local')
        .pipe(map((categories) => categories.data));
    }

    public createCategory(catInput: ICreateCategoryInput){
        return this.graphqlService.mutate(this.categoriesMutation.createCategory, {catInput}, 'local')
    }

    public deleteCategory(id: number){
        return this.graphqlService.mutate(this.categoriesMutation.deleteCategory, {id}, 'local')
    }
}