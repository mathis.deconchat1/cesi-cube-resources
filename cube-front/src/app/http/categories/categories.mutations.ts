import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class CategoriesMutation {
  public createCategory = gql`
    mutation createCategory($catInput: CreateCategoryInput!) {
      createCategory(input: $catInput) {
        category {
          name
          description
          id
        }
      }
    }
  `;

  public deleteCategory = gql`
    mutation deleteCategory($id: Int!) {
      deleteCategory(input: {id: $id}) {
        category {
          id
        }
      }
    }
  `;
}
