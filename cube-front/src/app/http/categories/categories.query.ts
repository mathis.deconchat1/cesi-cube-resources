import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class CategoriesQuery {
  public getCategories = gql`
    query categories {
      categories {
        nodes {
          id
          name
          description
        }
      }
    }
  `;
}