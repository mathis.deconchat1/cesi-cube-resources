import { PostsMutation } from './posts.mutation';


import { PostsQuery } from './posts.query';
import { GraphQLService } from '../graphql.service';
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { ICusCreateUpdatePostInput } from 'src/app/models/generated-maia.model';



@Injectable({
    providedIn: "root"
})
export class PostsGraphQL {
    constructor(
        private graphqlService : GraphQLService,
        private postsQuery : PostsQuery,
        private postsMutation: PostsMutation
        )  {}

    public getPartialPosts() {
        return this.graphqlService.query(this.postsQuery.getPartialPosts, {}, 'local')
        .pipe(map((partialPosts) => partialPosts.data));
    }

    public getPostDetail(id: number){
        return this.graphqlService.query(this.postsQuery.getPostDetails, {id}, 'local')
        .pipe(map((postDetail) => postDetail.data ))
    }

    public createPost(postInput: ICusCreateUpdatePostInput){
        return this.graphqlService.mutate(this.postsMutation.createPosts, {postInput}, 'local');
    }
}