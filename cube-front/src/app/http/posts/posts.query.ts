import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class PostsQuery {
  public getPartialPosts = gql`
    query getPartialPosts {
      posts {
        nodes {
          categorie {
            name
          }
          postTitle
          postDescription
          id
          createdAt
          userByCreatedBy {
            username
          }
          postsTags {
            nodes {
              tag {
                name
              }
            }
          }
        }
      }
    }
  `;

  public getPostDetails = gql`
    query getPostDetails($id: Int!) {
      isAdmin
      post(id: $id) {
        categorie {
          name
        }
        postContent
        createdAt
        postDescription
        postIsDeleted
        postTitle
        postsTags {
          nodes {
            tag {
              name
            }
            createdAt
          }
        }
        userByCreatedBy {
          username
        }
      }
    }
  `;
}
