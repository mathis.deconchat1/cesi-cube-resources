import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class PostsMutation {
  public createPosts = gql`
    mutation cus($postInput: CusCreateUpdatePostInput!) {
      cusCreateUpdatePost(input: $postInput) {
        clientMutationId
      }
    }
  `;
}
//test