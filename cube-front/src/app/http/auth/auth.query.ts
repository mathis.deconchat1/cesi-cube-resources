import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class AuthQuery {
  public isAdmin = gql`
    query MyQuery {
      isAdmin
    }
  `;
}
