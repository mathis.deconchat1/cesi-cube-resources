import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class AuthMutation {
  public createUser = gql`
    mutation createUserIfNotExist {
      createUserById(input: {}) {
        clientMutationId
      }
    }
  `;
}
