import { AuthMutation } from './auth.mutation';
import { GraphQLService } from './../graphql.service';
import { AuthQuery } from './auth.query';
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";


@Injectable({
    providedIn: 'root'
})

export class AuthGraphql{
    constructor(
        private authQuery: AuthQuery,
        private graphQLService: GraphQLService,
        private authMutation: AuthMutation
    ){}

    public isAdmin(){
        return this.graphQLService.query(this.authQuery.isAdmin, {}, 'local')
        .pipe(map((auth) => auth.data))
    }

    public createUserIfNotExist(){
        return this.graphQLService.mutate(this.authMutation.createUser, {}, 'local')
    }
}