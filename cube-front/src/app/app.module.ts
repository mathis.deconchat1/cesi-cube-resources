import { SafeHtmlPipe } from './pipes/unsanitize.pipe';
import { AuthService } from './services/auth.service';
import { GraphQLModule } from './graphql.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { QuillModule } from 'ngx-quill'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomepagePage } from './pages/homepage/homepage.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MaterialModule } from './modules/material/material.module';
import { HeroBannerComponent } from './components/homepage/hero-banner/hero-banner.component';
import { TopicNavComponent } from './components/homepage/topic-nav/topic-nav.component';
import { ArticleCardComponent } from './components/articles/article-card/article-card.component';
import { FooterComponent } from './components/footer/footer.component';
import { AsideCreateArticleComponent } from './components/homepage/aside-create-article/aside-create-article.component';
import { ArticleCreationPage } from './pages/article-creation/article-creation.page';
import { HttpClientModule } from '@angular/common/http';
import {HttpLink} from 'apollo-angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticlesDetailsPage } from './pages/articles-details/articles-details.page';
import { AsideInfoComponent } from './components/articles/aside-info/aside-info.component';
import { AdminPage } from './pages/admin/admin.page';
import { StatsComponent } from './components/admin/stats/stats.component';
import { BannerComponent } from './components/admin/banner/banner.component';
import { AdmCategoriesComponent } from './components/admin/adm-categories/adm-categories.component';
import { AuthGuard } from './services/auth.guard';
import { AdmTagsComponent } from './components/admin/adm-tags/adm-tags.component';
import { AdmPostsComponent } from './components/admin/adm-posts/adm-posts.component';



@NgModule({
  declarations: [
    AppComponent,
    HomepagePage,
    NavbarComponent,
    HeroBannerComponent,
    TopicNavComponent,
    ArticleCardComponent,
    FooterComponent,
    AsideCreateArticleComponent,
    ArticleCreationPage,
    ArticlesDetailsPage,
    AsideInfoComponent,
    AdminPage,
    StatsComponent,
    BannerComponent,
    AdmCategoriesComponent,
    AdmTagsComponent,
    SafeHtmlPipe,
    AdmPostsComponent,
    
    
  ],
  imports: [
    BrowserModule,
    GraphQLModule,
    HttpClientModule,  
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
    
  ],
  providers: [HttpClientModule, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
