import { AuthGraphql } from './../http/auth/auth.graphql';
import { Injectable, OnInit } from '@angular/core';
import { firstValueFrom, lastValueFrom, map, Subject, take, tap } from 'rxjs';
@Injectable()
export class AuthService {
  constructor(private authGraphql: AuthGraphql) {}
  auth: boolean = false;
  sub = new Subject<boolean>();

  public isAuthenticated() {
    console.log('start');
    return this.authGraphql.isAdmin().pipe(
      tap((res: any) => {
        console.log(res);
      }),
      map((res) => res.isAdmin)
    );
  }
}
