import { AuthGraphql } from './http/auth/auth.graphql';
import { AuthService } from './services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'cesi-cube-resource';
  constructor(
    private authService: AuthService,
    private authGraphql: AuthGraphql
    ) {}
  ngOnInit(): void {
    this.authGraphql.createUserIfNotExist().subscribe(e => {
    })
    
  }
}
//app.component.ts
// test pipelinz
