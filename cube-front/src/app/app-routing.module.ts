import { AdminPage } from './pages/admin/admin.page';
import { ArticlesDetailsPage } from './pages/articles-details/articles-details.page';
import { HomepagePage } from './pages/homepage/homepage.page';
import { ArticleCreationPage } from './pages/article-creation/article-creation.page';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'create', component: ArticleCreationPage },
  { path: 'home', component: HomepagePage },
  { path: 'article/:id', component: ArticlesDetailsPage},
  { path: 'admin', component: AdminPage }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
