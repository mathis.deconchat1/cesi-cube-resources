"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const postgraphile_1 = require("postgraphile");
const pg_simplify_inflector_1 = __importDefault(require("@graphile-contrib/pg-simplify-inflector"));
const pg_many_to_many_1 = __importDefault(require("@graphile-contrib/pg-many-to-many"));
const postgraphile_plugin_connection_filter_1 = __importDefault(require("postgraphile-plugin-connection-filter"));
const cors_1 = __importDefault(require("cors"));
const jwt_decode_1 = __importDefault(require("jwt-decode"));
const options = {
    origin: "*",
};
let allowCrossDomain = function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
};
dotenv_1.default.config();
const DATABASE_URL = process.env.URL;
const SCHEMA_NAMES = process.env.SCHEMA_NAMES;
console.log(DATABASE_URL, SCHEMA_NAMES);
const postgraphileOpt = {
    watchPg: true,
    graphiql: true,
    enhanceGraphiql: true,
    exportJsonSchemaPath: "schema.json",
    exportGqlSchemaPath: "schema.graphql",
    appendPlugins: [
        pg_simplify_inflector_1.default,
        pg_many_to_many_1.default,
        postgraphile_plugin_connection_filter_1.default,
    ],
    graphileBuildOptions: {
        connectionFilterRelations: true,
    },
    pgSettings: (req) => __awaiter(void 0, void 0, void 0, function* () {
        const settings = {};
        let ressourceRole = "";
        settings["role"] = "ressource_user_unallowed";
        let token = "";
        if (!!req.headers["x-id-token"]) {
            token = req.headers["x-id-token"];
        }
        if (!!token) {
            const decoded = jwt_decode_1.default(token, { header: true });
            console.log(decoded);
            const roles = decoded.roles;
            if (!!roles) {
                if (roles.includes("ressource_user")) {
                    ressourceRole = "ressource_user";
                }
            }
            if (roles.includes("ressource_admin")) {
                ressourceRole = "ressource_admin";
            }
            settings["role"] = ressourceRole;
            settings["token.user_id"] = decoded.sub;
            settings["token.user_name"] = decoded.preferred_username;
            settings["token.email"] = decoded.email;
        }
        console.log(settings);
        return settings;
    }),
};
const app = express_1.default();
app.use(cors_1.default());
app.use(allowCrossDomain);
app.use(postgraphile_1.postgraphile(DATABASE_URL, SCHEMA_NAMES, postgraphileOpt));
app.listen(6060);
