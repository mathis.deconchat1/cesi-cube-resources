import dotenv from "dotenv";
import express from "express";
import { postgraphile } from "postgraphile";
import PgSimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";
import PgManyToManyPlugin from "@graphile-contrib/pg-many-to-many";
import ConnectionFilterPlugin from "postgraphile-plugin-connection-filter";
import cors from "cors";
import jwt_decode from "jwt-decode";

const options = {
  origin: "*",
};

//cors 
let allowCrossDomain = function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
  );

  next();
};

dotenv.config();

const DATABASE_URL = process.env.URL;

const SCHEMA_NAMES = process.env.SCHEMA_NAMES;
console.log(DATABASE_URL, SCHEMA_NAMES);
const postgraphileOpt = {
  watchPg: true,
  graphiql: true,
  enhanceGraphiql: true,
  exportJsonSchemaPath: "schema.json",
  exportGqlSchemaPath: "schema.graphql",

  appendPlugins: [
    PgSimplifyInflectorPlugin,
    PgManyToManyPlugin,
    ConnectionFilterPlugin,
  ],
  graphileBuildOptions: {
    connectionFilterRelations: true,
  },

  pgSettings: async (req) => {
    const settings = {};
    let ressourceRole: string = "";
    settings["role"] = "ressource_user_unallowed";

    let token: string = "";

    if (!!req.headers["x-id-token"]) {
      token = req.headers["x-id-token"];
    }

    if (!!token) {
      const decoded: any = jwt_decode(token, { header: true });
      console.log(decoded);
      const roles: string[] = decoded.roles;
      if (!!roles) {
        if (roles.includes("ressource_user")) {
          ressourceRole = "ressource_user";
        }
      }
      if (roles.includes("ressource_admin")) {
        ressourceRole = "ressource_admin";
      }
      settings["role"] = ressourceRole;
      settings["token.user_id"] = decoded.sub;
      settings["token.user_name"] = decoded.preferred_username;
      settings["token.email"] = decoded.email;
    }

    console.log(settings);
    return settings;
  },
};

const app = express();
app.use(cors());
app.use(allowCrossDomain);
app.use(postgraphile(DATABASE_URL, SCHEMA_NAMES, postgraphileOpt));

app.listen(6060);
