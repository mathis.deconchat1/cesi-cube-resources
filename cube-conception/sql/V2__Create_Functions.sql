CREATE OR REPLACE FUNCTION ressource_public.cus_create_update_post(i_post ressource_public.posts, tags_id integer[] DEFAULT NULL::integer[])
 RETURNS ressource_public.posts
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare r_post ressource_public.posts;
BEGIN
   if i_post.id is not null then
     update ressource_public.posts 
        set post_title = i_post.post_title,
        post_content = i_post."content",
        post_status = i_post.status,
        post_description = i_post.post_description,
        categorie_id = i_post.categorie_id 
        where id = i_post.id 
        returning * into r_post;
     if r_post is null then
       raise exception 'That posts was not found' using errcode = 'NOT FOUND';
     end if;

   else
     INSERT INTO ressource_public.posts(post_title, post_description, post_content, categorie_id, created_by) 
     VALUES (i_post.post_title, i_post.post_description,i_post.post_content, i_post.categorie_id, current_setting('token.user_id')::uuid)
     returning * into r_post; 
   end if;
 
   delete from ressource_public.posts_tags as pt 
            where pt.post_id  = r_post.id
            and pt.tag_id <> all (tags_id);
   insert into ressource_public.posts_tags (post_id, tag_id)
      select r_post.id, tag
      from unnest(tags_id) tag
      on conflict do nothing;

return r_post;
END;
$function$
;

CREATE OR REPLACE FUNCTION ressource_public.create_user_by_id()
 RETURNS void
 LANGUAGE plpgsql
 AS $$
 BEGIN
 	  IF NOT EXISTS (
    SELECT
      1
    FROM
      ressource_public.users AS u
    WHERE
      u.id = current_setting('token.user_id')::uuid
      ) THEN
  INSERT INTO ressource_public.users (id, username, email) values (current_setting('token.user_id')::uuid, current_setting('token.user_name'), current_setting('token.email'));
END IF;
 END
 $$
 ;


create or replace function ressource_public.cus_create_tag(i_tag ressource_public.tags)
returns ressource_public.tags
language plpgsql
security definer
as
$function$
begin
    insert into ressource_public.tags (name) values (i_tag.name) on conflict do nothing ;
end;
$function$;


create unique index tags_name_idx on ressource_public.tags using btree (name);

create or replace function ressource_public.cus_create_tag(i_tag ressource_public.tags)
returns ressource_public.tags
language plpgsql
security definer
as
$function$
    declare r_tag ressource_public.tags;
begin
    if not exists(
        select 1 from ressource_public.tags where name = i_tag.name
    ) then
        insert into ressource_public.tags (name) values (i_tag.name)
        on conflict do nothing
        returning * into r_tag;
    else
    select * from ressource_public.tags where name = i_tag.name
     into r_tag;
    end if;

    return r_tag;


end;
$function$;