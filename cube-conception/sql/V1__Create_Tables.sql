begin;

create schema ressource_public;

create extension if not exists "uuid-ossp";

create type ressource_public.t_post_status as enum ('draft', 'published');

create type ressource_public.t_posts_users_actions as enum (
    'create',
    'update',
    'delete',
    'moderate'
);

create table ressource_public.categories (
    id serial primary key,
    name varchar(255) not null check (char_length(name) < 100),
    description text not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
);

create table ressource_public.tags(
    id serial primary key,
    name varchar(255) not null check (char_length(name) < 100),
    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
);

create table ressource_public.users (
    id uuid primary key,
    username varchar(255) not null check (char_length(username) < 100),
    email varchar(255) not null check (char_length(email) < 100)
);

create table ressource_public.posts(
    id serial primary key,
    post_title varchar(255) not null check (char_length(post_title) < 100),
    post_description text,
    post_content text default 1,
    post_is_deleted boolean not null default false,
    post_status ressource_public.t_post_status not null default 'draft',
    categorie_id int,
    created_by uuid,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    foreign key (categorie_id) references ressource_public.categories (id) on delete cascade,
    foreign key (created_by) references ressource_public.users (id) on delete cascade
);


create table ressource_public.posts_tags(
    id serial primary key,
    post_id int not null,
    tag_id int not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    foreign key (post_id) references ressource_public.posts (id) on delete cascade,
    foreign key (tag_id) references ressource_public.tags (id) on delete cascade
);

CREATE UNIQUE INDEX posts_tags_idx ON ressource_public.posts_tags USING btree (post_id, tag_id);


-- Not used
create table ressource_public.posts_users_actions(
    id serial primary key,
    post_id int not null,
    user_id uuid not null,
    action ressource_public.t_posts_users_actions not null,
    updated_at timestamp not null default now(),
    foreign key (post_id) references ressource_public.posts (id) on delete cascade,
    foreign key (user_id) references ressource_public.users (id) on delete cascade
);

CREATE UNIQUE INDEX post_tag_idx ON ressource_public.posts_tags  USING btree (post_id, tag_id);
