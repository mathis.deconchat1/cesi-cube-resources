create role ressource_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA ressource_public TO ressource_user;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA ressource_public TO ressource_user;
GRANT EXECUTE ON ALL ROUTINES IN SCHEMA ressource_public TO ressource_user;
GRANT USAGE ON SCHEMA ressource_public TO ressource_user;

create role ressource_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA ressource_public TO ressource_admin;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA ressource_public TO ressource_admin;
GRANT EXECUTE ON ALL ROUTINES IN SCHEMA ressource_public TO ressource_admin;
GRANT USAGE ON SCHEMA ressource_public TO ressource_admin;