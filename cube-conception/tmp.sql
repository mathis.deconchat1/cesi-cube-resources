CREATE OR REPLACE FUNCTION ressource_public.cus_create_update_post(i_post ressource_public.posts, tags_id integer[] DEFAULT NULL::integer[])
 RETURNS ressource_public.posts
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare r_post ressource_public.posts;
BEGIN
   if i_post.id is not null then
     update ressource_public.posts 
        set post_title = i_post.post_title,
        post_content = i_post."content",
        post_status = i_post.status,
        post_description = i_post.post_description,
        categorie_id = i_post.categorie_id 
        where id = i_post.id 
        returning * into r_post;
     if r_post is null then
       raise exception 'That posts was not found' using errcode = 'NOT FOUND';
     end if;

   else
     INSERT INTO ressource_public.posts(post_title, post_description, post_content, categorie_id, created_by) 
     VALUES (i_post.post_title, i_post.post_description,i_post.content, i_post.categorie_id, current_setting('token.user_id'))
     returning * into r_post; 
   end if;
 
   delete from ressource_public.posts_tags as pt 
            where pt.post_id  = r_post.id
            and pt.tag_id <> all (tags_id);
   insert into ressource_public.posts_tags (post_id, tag_id)
      select r_post.id, tag
      from unnest(tags_id) tag
      on conflict do nothing;

return todor;
END;
$function$